package com.nw.uploadingfiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsUploadingFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsUploadingFilesApplication.class, args);
	}

}
